@extends('layouts.admin')

@section('content')

<div class="container">
  <h2>{{$title}}</h2>
  <form action="" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
    <div class="form-group col-md-6">
      <label for="amount">Amount:</label>
      <input type="text" class="form-control" value="{{ @$invoice->amount ?? @old('amount')}}" id="amount" placeholder="Enter Amount" name="amount">
      @if ($errors->has('amount'))
          <span class="text-danger">{{ $errors->first('amount') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="discount">discount:</label>
      <input type="text" class="form-control" value="{{ @$invoice->discount ?? @old('discount')}}" id="discount" placeholder="Enter discount" name="discount">
      @if ($errors->has('discount'))
          <span class="text-danger">{{ $errors->first('discount') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="gstin">GSTIN:</label>
      <input type="text" class="form-control" id="gstin" value="{{ @$invoice->gstin ?? @old('gstin')}}" placeholder="Enter gstin" name="gstin">
      @if ($errors->has('gstin'))
          <span class="text-danger">{{ $errors->first('gstin') }}</span>
      @endif
    </div>
    
    <div class="form-group col-md-6">
      <label for="placeofsupply">place of supply:</label>
      <input type="placeofsupply" class="form-control"  value="{{ @$invoice->placeofsupply ?? @old('placeofsupply')}}" id="placeofsupply" placeholder="Enter placeofsupply" name="placeofsupply">
      @if ($errors->has('placeofsupply'))
          <span class="text-danger">{{ $errors->first('placeofsupply') }}</span>
      @endif
    </div>
    <div class="form-group col-md-6">
      <label for="delivery_address">delivery_address:</label>
      <input type="text" class="form-control" id="delivery_address"  value="{{ @$invoice->delivery_address ?? @old('delivery_address')}}" placeholder="Enter delivery_address" name="delivery_address">
      @if ($errors->has('delivery_address'))
          <span class="text-danger">{{ $errors->first('delivery_address') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="city">city:</label>
      <input type="text" class="form-control" id="city"  value="{{ @$invoice->city ?? @old('city')}}" placeholder="Enter city" name="city">
      @if ($errors->has('city'))
          <span class="text-danger">{{ $errors->first('city') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="state">state:</label>
      <input type="text" class="form-control" id="state"  value="{{ @$invoice->state ?? @old('state')}}" placeholder="Enter state" name="state">
      @if ($errors->has('state'))
          <span class="text-danger">{{ $errors->first('state') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="pincode">pincode:</label>
      <input type="text" class="form-control" id="pincode"  value="{{ @$invoice->pincode ?? @old('pincode')}}" placeholder="Enter pincode" name="pincode">
      @if ($errors->has('pincode'))
          <span class="text-danger">{{ $errors->first('pincode') }}</span>
      @endif
    </div>
    
    
  
  </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

<div class="container">
<h2>Invoice List</h2>  

  @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
  @endif  
  


   
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>amount</th>
        <th>discount</th>
        <th>place of supply</th>
        <th>delivery address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($invoices as $row)
      <tr>
        <td>{{@$row->amount}}</td>
        <td>{{@$row->discount}}</td>
        <td>{{@$row->placeofsupply}}</td>
        <td>{{@$row->delivery_address}}</td>
        <td> <a href="{{url('admin/invoice/edit/'.$row->id)}}">Edit</a> | <a href="{{url('admin/invoice/delete/'.$row->id)}}"  onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
</div>
<script>

function is_delete(){
  if(confirm('Are you Sure? You want to delete this record.')){
    return true;
  }
  return false;
}
</script>
@endsection

