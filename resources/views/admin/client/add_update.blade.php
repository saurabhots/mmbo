@extends('layouts.admin')

@section('content')

<div class="container">
  <h2>{{$title}}</h2>
  <form action="" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
    <div class="form-group col-md-6">
      <label for="name">Client name:</label>
      <input type="text" class="form-control" value="{{ @$client->name ?? @old('name')}}" id="name" placeholder="Enter name" name="name">
      @if ($errors->has('name'))
          <span class="text-danger">{{ $errors->first('name') }}</span>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="email">Email address:</label>
      <input type="email" class="form-control" id="email" value="{{ @$client->email ?? @old('email')}}" placeholder="Enter email" name="email">
      @if ($errors->has('email'))
          <span class="text-danger">{{ $errors->first('email') }}</span>
      @endif
    </div>
    
    <div class="form-group col-md-6">
      <label for="mobile">Contact number:</label>
      <input type="mobile" class="form-control"  value="{{ @$client->mobile ?? @old('mobile')}}" id="mobile" placeholder="Enter mobile" name="mobile">
      @if ($errors->has('mobile'))
          <span class="text-danger">{{ $errors->first('mobile') }}</span>
      @endif
    </div>
    <div class="form-group col-md-6">
      <label for="address">Address:</label>
      <input type="text" class="form-control" id="address"  value="{{ @$client->address ?? @old('address')}}" placeholder="Enter address" name="address">
      @if ($errors->has('address'))
          <span class="text-danger">{{ $errors->first('address') }}</span>
      @endif
    </div>
    
    
  
  </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

<script type="text/javascript">
  function duplicateEmail(element){
        var email = $(element).val();
        if(email == ''){
          document.getElementById("success").innerHTML = '';
          document.getElementById("error").innerHTML = '';
        } else {
        $.ajax({
            type: "GET",
            url: '{{url('checkemail')}}',
            data: {email:email},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                    document.getElementById("success").innerHTML = '';
                    document.getElementById("error").innerHTML = 'Email already taken';
                }else{
                    document.getElementById("error").innerHTML = '';
                    document.getElementById("success").innerHTML = 'Email validate';
                }
            },
            error: function (jqXHR, exception) {

            }
        });
      }
    }
</script>
@endsection
