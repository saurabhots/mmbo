@extends('layouts.admin')

@section('content')

<div class="container">
  <h2>{{$title}}</h2>  

  <a href="{{url('admin/client/create')}}"> Add Client</a>  
  @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
  @endif  
  


   
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($clients as $row)
      <tr>
        <td>{{$row->name}}</td>
        <td>{{$row->email}}</td>
        <td>{{$row->mobile}}</td>
        <td>{{$row->address}}</td>
        <td> <a href="{{url('admin/client/edit/'.$row->id)}}">Edit</a> | <a href="{{url('admin/client/delete/'.$row->id)}}"  onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>  | <a href="{{url('admin/invoice/create/'.$row->id)}}">Add Invoice</a></td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
</div>
<script>

function is_delete(){
  if(confirm('Are you Sure? You want to delete this record.')){
    return true;
  }
  return false;
}
</script>
@endsection

