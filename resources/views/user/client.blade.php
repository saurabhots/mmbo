
@extends('layouts.user')

@section('content')
<div class="container">
  <h2>{{$title}}</h2>  

  @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
  @endif  
  


   
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($clients as $row)
      <tr>
        <td>{{$row->name}}</td>
        <td>{{$row->email}}</td>
        <td>{{$row->mobile}}</td>
        <td>{{$row->address}}</td>
        <td> <a href="{{url('user/invoice/'.$row->id)}}">Invoice</a></td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
</div>

@endsection

