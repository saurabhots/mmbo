@extends('layouts.user')

@section('content')
<div class="container">
<h2>Invoice List</h2>  

  @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
  @endif  
  


   
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>amount</th>
        <th>discount</th>
        <th>place of supply</th>
        <th>delivery address</th>
      </tr>
    </thead>
    <tbody>
      @foreach($invoices as $row)
      <tr>
        <td>{{@$row->amount}}</td>
        <td>{{@$row->discount}}</td>
        <td>{{@$row->placeofsupply}}</td>
        <td>{{@$row->delivery_address}}</td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
</div>

@endsection

