<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;

class InvoiceController extends Controller
{
 
    public function index($id){
        $invoices = Invoice::where('client_id',$id)->orderby('id','desc')->get();
        $title = 'Invoice List';
        return view('user.invoice',compact('invoices','title'));
    }

}

