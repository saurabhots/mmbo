<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;

class ClientController extends Controller
{
    public function index(){
        $clients = Client::orderby('id','desc')->get();
        $title = 'Client List';
        return view('user.client',compact('clients','title'));
    }
    
}

