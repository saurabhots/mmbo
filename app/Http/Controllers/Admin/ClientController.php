<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;

class ClientController extends Controller
{
    public function index(){
        $clients = Client::orderby('id','desc')->get();
        $title = 'Client List';
        return view('admin.client.index',compact('clients','title'));
    }



    public function create(Request $request){
        if($request->all()){
            $request->validate([

                'name' => 'required',
                'email' => 'required|email|unique:clients',
                'mobile' => 'required|unique:clients',
                'address' => 'required',
                
            ]);

            

            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'address' => $request->address,
                
            );
            Client::create($data);
            return redirect('admin/client')->with('success', 'Client has been created successfully.');

        } else {
           $title = 'Add Client';
           return view('admin.client.add_update',compact('title')); 
        }
    }

    public function edit(Request $request,$id){
        $client = Client::where('id',$id)->first();
        if($request->all()){
            $request->validate([
                'name' => 'required',
                'email' => 'required|unique:clients,email,'.$id,
                'mobile' => 'required|unique:clients,mobile,'.$id,
                'address' => 'required',
            ]);


            

            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'address' => $request->address,
            );
            $client->update($data);
            return redirect('admin/client')->with('success', 'Client has been updated successfully.');

        } else {
           
           $title = 'Edit Client';
           return view('admin.client.add_update',compact('title','client')); 
        }
    }


    public function delete($id){
        Client::where('id',$id)->delete();
        return redirect('admin/client')->with('success', 'Client has been deleted successfully.');
    }

    
}

