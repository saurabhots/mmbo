<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;

class InvoiceController extends Controller
{
 
    public function index(){
        $invoice = Invoice::orderby('id','desc')->get();
        $title = 'Invoice List';
        return view('admin.invoice.index',compact('invoices','title'));
    }



    public function create(Request $request,$id){
        if($request->all()){
            $request->validate([

                'amount' => 'required',
                'gstin' => 'required',
                'delivery_address' => 'required',
                'placeofsupply' => 'required',
                
            ]);

            

            $data = array(
                'client_id' => $id,
                'amount' => $request->amount,
                'gstin' => $request->gstin,
                'placeofsupply' => $request->placeofsupply,
                'discount' => $request->discount,
                'delivery_address' => $request->delivery_address,
                'city' => $request->city,
                'state' => $request->state,
                'pincode' => $request->pincode,

            );
            Invoice::create($data);
            return redirect('admin/invoice/create/'.$id)->with('success', 'Invoice has been created successfully.');

        } else {
           $title = 'Add Invoice';
           $invoices = Invoice::orderby('id','desc')->get(); 
           return view('admin.invoice.index',compact('title','invoices')); 
        }
    }

    public function edit(Request $request,$id){
        $invoice = Invoice::where('id',$id)->first();
        if($request->all()){
            $request->validate([
                'amount' => 'required',
                'gstin' => 'required',
                'delivery_address' => 'required',
                'placeofsupply' => 'required',
            ]);


            

           $data = array(
                'amount' => $request->amount,
                'gstin' => $request->gstin,
                'placeofsupply' => $request->placeofsupply,
                'discount' => $request->discount,
                'delivery_address' => $request->delivery_address,
                'city' => $request->city,
                'state' => $request->state,
                'pincode' => $request->pincode,

            );
/*print_r($invoice);die;*/
            $invoice->update($data);
            return redirect('admin/invoice/create/'.$invoice->client_id)->with('success', 'Invoice has been updated successfully.');

        } else {
           
           $title = 'Edit Invoice';
           $invoices = Invoice::orderby('id','desc')->get(); 
           return view('admin.invoice.index',compact('title','invoices','invoice')); 
        }
    }


    public function delete($id){
        Invoice::where('id',$id)->delete();
        return redirect('admin/invoice/create/'.$id)->with('success', 'Invoice has been deleted successfully.');
    }

    
}

