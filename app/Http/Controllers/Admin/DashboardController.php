<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function index(){
        $user =  Auth::user();
        return view('admin.dashboard',compact('user'));
    }
}
