<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{

    use SoftDeletes;
    protected $table = 'clients';

    protected $fillable = [
        'name', 'email', 'mobile', 'address',
    ];

    public function invoice(){
        return $this->hasMany('App\Invoice');
    }
}
