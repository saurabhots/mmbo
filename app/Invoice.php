<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{

    use SoftDeletes;
    protected $table = 'invoices';

    protected $fillable = [
        'client_id', 'amount', 'gstin', 'placeofsupply', 'discount', 'delivery_address', 'city', 'state', 'pincode',
    ];

    public function client(){
       return $this->belongsTo('App\Client');
    }

}
