<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');
Route::group(['as'=>'admin.','prefix' => 'admin','namespace'=>'Admin','middleware'=>['auth','admin']], function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        /*client route*/
        Route::get('client', 'ClientController@index')->name('client');
        Route::any('client/create', 'ClientController@create')->name('client.create');
        Route::any('client/edit/{id}', 'ClientController@edit')->name('client.edit');
        Route::any('client/delete/{id}', 'ClientController@delete')->name('client.delete');

        /*Invoices route*/
        Route::get('invoice', 'InvoiceController@index')->name('invoice');
        Route::any('invoice/create/{id}', 'InvoiceController@create')->name('invoice.create');
        Route::any('invoice/edit/{id}', 'InvoiceController@edit')->name('invoice.edit');
        Route::any('invoice/delete/{id}', 'InvoiceController@delete')->name('invoice.delete');
});

Route::group(['as'=>'user.','prefix' => 'user','namespace'=>'User','middleware'=>['auth','user']], function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('client', 'ClientController@index')->name('client');
        Route::any('invoice/{id}', 'InvoiceController@index')->name('invoice');
});
